package com.example.demo.mapper;

import com.example.demo.model.UserPro;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * Created by qfcomputer on 2018/6/6.
 */

@Repository
public interface UserProMapper {

    @Select("select user.id,user.name,projectName from test.user left join test1.project on user.`projectId`=project.id limit 1")
    UserPro findProName();
}
