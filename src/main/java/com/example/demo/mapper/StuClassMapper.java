package com.example.demo.mapper;

import com.example.demo.model.StuClass;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface StuClassMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StuClass record);

    int insertSelective(StuClass record);

    StuClass selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StuClass record);

    int updateByPrimaryKey(StuClass record);


}