package com.example.demo.mapper;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    @Select("select * from test.user where id in(select userId from test1.stuClass) limit 1")
    User findByUserId();

    @Select("select count(*) from test.user left join test1.stuClass on user.id=stuClass.userId")
    int findByUserIdJoin();

    @Select("select stuClass.userId from test.user left join test1.stuClass on user.id=stuClass.userId limit 1")
    String findUserNameByUserIdJoin();
}