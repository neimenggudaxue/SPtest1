package com.example.demo;

import com.example.demo.mapper.ProjectMapper;
import com.example.demo.mapper.StuClassMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.mapper.UserProMapper;
import com.example.demo.model.StuClass;
import com.example.demo.model.User;
import com.example.demo.model.UserPro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by qfcomputer on 2018/6/6.
 */

@Controller
public class testController {

    @Autowired
    private StuClassMapper stuClassMapper;
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserProMapper userProMapper;

    @RequestMapping("/test")
    @ResponseBody
    public String  getStuClass(){
        User user=userMapper.findByUserId();
        System.out.println(user.getName());
        return user.getName();
    }

    @RequestMapping("/test2")
    @ResponseBody
    public String  getStuClassByJoin(){

        System.out.println(userMapper.findByUserIdJoin());
        return "userMapper.findByUserIdJoin()";
    }

    @RequestMapping("/test3")
    @ResponseBody
    public UserPro getProName(){

        UserPro userPro=userProMapper.findProName();
        return userPro;
    }

    @RequestMapping("/test4")
    @ResponseBody
    public String  findUserNameByUserIdJoin(){
        return userMapper.findUserNameByUserIdJoin();
    }

}
