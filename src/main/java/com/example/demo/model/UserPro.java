package com.example.demo.model;

/**
 * Created by qfcomputer on 2018/6/6.
 */

public class UserPro {
    private Integer id;
    private String name;
    private String projectname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

}
